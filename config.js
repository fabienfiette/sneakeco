const mysql = require("mysql");

const config = {
  dev: {
    host: 'localhost',
    user: 'root',
    password: 'mysqlRootPassword',
    database: 'sneakecoDev'
  },
  prod: {
    host: 'us-cdbr-east-06.cleardb.net',
    user: 'b8d5fff0e55e83',
    password: '228f59a0',
    database: 'heroku_b10bb0efa811ecc'
  }
};

const connection = mysql.createConnection(config[process.argv[2] || config.dev]);

connection.connect(err => {
  if (err) throw err;
  console.log("Connected to mysql db")
});

setInterval(() => {
  connection.query('SELECT 1')
}, 5000)

module.exports = connection;
