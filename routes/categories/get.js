const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT c.* FROM categories as c ORDER BY c.id;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/categorie/:id", (req, res) => {
  mysqldb.query(`SELECT c.* FROM categories as c WHERE c.id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

module.exports = router;
