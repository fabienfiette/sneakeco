const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.post("/", (req, res) => {
  mysqldb.query(`INSERT INTO categories (name) VALUES ('${req.body.name}');`,
    (err, result) => {
      if (err) { res.status(500).send(err) };
      res.send(result);
    })
});

module.exports = router;
