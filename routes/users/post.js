const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

const jwtsecret = 'sneakecoProject751064';
const jwt = require('jsonwebtoken');
const bcrypt = require("bcryptjs");
const salt = "$2a$10$5UTQoU4Fh6.7oHE7Qyn6Vu";

router.post("/", (req, res) => {
  mysqldb.query(`INSERT INTO users (first_name, phone_num, email, password, adress, city, postal_code) VALUES ('${req.body.firstName}','${req.body.phoneNum}','${req.body.email}','${bcrypt.hashSync(req.body.password, salt)}','${req.body.address}','${req.body.city}','${req.body.postalCode}');`,
    (err, result) => {
      if (err) { res.status(500).send(err) };
      res.send(result);
    })
});

router.post("/signin", (req, res) => {

  // on récupère les données de connexion de notre utilisateur 
  const users = req.body;
  const crypted = bcrypt.hashSync(users.password, salt);

  // on va essayer de trouver dans la base de données un utilisateur 
  // dont les noms et mots de passe correspondent

  mysqldb.query(
    `SELECT * FROM users as u WHERE u.email = '${users.email}' AND u.password = '${crypted}';`,
    (err, result) => {
      if (err) {
        res.send("Erreur");
      } else if (result.length === 0) {
        res.send("Resultat vide");
      } else {
        // token creation
        jwt.sign(
          // result[0],
          { users },
          jwtsecret,
          (err, token) => {
            if (err) {
              res.status(501).send("JWT error");
            }
            else {
              res.json([{ ...result[0], token }]);
            }
          })
      }
    }
  )

});

module.exports = router;
