const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT u.* FROM users as u ORDER BY u.id;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/user/:id", (req, res) => {
  mysqldb.query(`SELECT u.* FROM users as u WHERE u.id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

module.exports = router;
