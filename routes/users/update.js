const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

const bcrypt = require("bcryptjs");
const verifyToken = require("../../verifyToken");
const salt = "$2a$10$5UTQoU4Fh6.7oHE7Qyn6Vu";

router.put("/update/:id", verifyToken, (req, res) => {
  const { firstName, phoneNum, email, password, address, city, postalCode } = req.body;
  const idUser = req.params.id;
  mysqldb.query(`UPDATE users SET ${firstName && `first_name = "${firstName}",`} ${phoneNum && `phone_num = "${phoneNum}",`} ${email && `email = "${email}",`} ${password && `password = "${bcrypt.hashSync(password, salt)}",`} ${address && `adress = "${address}",`} ${city && `city = "${city}",`} ${postalCode && `postal_code = "${postalCode}"`} WHERE id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    mysqldb.query(`SELECT u.* FROM users as u WHERE u.id = ${idUser};`, (err2, result2) => {
      if (err2) { res.send(err2) };
      res.json(result2);
    })
  })
});

module.exports = router;
