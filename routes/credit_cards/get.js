const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT cd.* FROM credit_cards as cd ORDER BY cd.id;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/credit_card/:id", (req, res) => {
  mysqldb.query(`SELECT cd.* FROM credit_cards as cd WHERE cd.id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/credit_card/user/:id", (req, res) => {
  mysqldb.query(`SELECT cd.* FROM credit_cards as cd WHERE cd.user_id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

module.exports = router;
