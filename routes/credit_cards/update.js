const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.put("/update/:id", (req, res) => {
  const { cardNum, expiration, name, cvcCode, userId } = req.body;
  mysqldb.query(`UPDATE credit_cards SET card_num = "${cardNum}", expiration = "${expiration}", name = "${name}", cvc_code = "${cvcCode}", user_id = ${userId} WHERE id = ${req.params.id};`, (err) => {
    if (err) { res.status(500).send(err) };
    mysqldb.query(`SELECT cd.* FROM credit_cards as cd WHERE cd.id = ${req.params.id};`, (err2, result2) => {
      if (err2) { res.send(err2) };
      res.json(result2);
    })
  })
});

module.exports = router;
