const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT p.*, c.name as "cat_name" FROM products as p, categories as c WHERE p.categorie_id = c.id ORDER BY p.id;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/product/:id", (req, res) => {
  mysqldb.query(`SELECT p.*, c.name as "cat_name" FROM products as p, categories as c WHERE p.categorie_id = c.id AND p.id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

module.exports = router;
