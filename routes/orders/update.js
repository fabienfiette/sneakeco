const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.put("/update/:id", (req, res) => {
  const { idProduct, idOrder, nbProduct } = req.body;
  mysqldb.query(`UPDATE orders SET ${idProduct ? `id_product = ${idProduct},` : ''} ${idOrder ? `id_order = ${idOrder},` : ''} ${nbProduct ? `nb_product = ${nbProduct}` : ''} WHERE id = ${req.params.id};`, (err) => {
    if (err) { res.status(500).send(err) };
    mysqldb.query(`SELECT o.* FROM orders as o WHERE o.id = ${req.params.id};`, (err2, result2) => {
      if (err2) { res.send(err2) };
      res.json(result2);
    })
  })
});

module.exports = router;
