const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.post("/", (req, res) => {
  mysqldb.query(`INSERT INTO orders (date, delivered, user_id) VALUES ('${req.body.date}', ${req.body.delivered}, ${req.body.userId});`,
    (err, result) => {
      if (err) { res.status(500).send(err) };
      if (req.body.cart) {
        req.body.cart.forEach(item => {
          mysqldb.query(`INSERT INTO orders_products (id_product, id_order, nb_product) VALUES (${item.id}, ${result.insertId}, ${item.nbProduct});`,
            (err2) => {
              if (err2) { res.status(500).send(err2) };
            })
        });
      }
      res.send(result);
    })
});

module.exports = router;
