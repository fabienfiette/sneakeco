const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT o.* FROM orders as o ORDER BY o.id;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/order/:id", (req, res) => {
  mysqldb.query(`SELECT o.* FROM orders as o WHERE o.id = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/order/user/:userId/:orderId", (req, res) => {
  mysqldb.query(`SELECT o.* FROM orders as o WHERE o.user_id = ${req.params.userId} AND o.id = ${req.params.orderId};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    mysqldb.query(`SELECT op.nb_product, p.* FROM orders_products as op, products as p WHERE op.id_order = ${req.params.orderId} AND p.id = op.id_product;`, (err2, result2) => {
      if (err2) { res.status(500).send(err2) };
      res.json([{
        ...result[0],
        items: [
          ...result2
        ]
      }])
    })
  })
});

module.exports = router;
