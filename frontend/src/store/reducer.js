import { configureStore } from "@reduxjs/toolkit";
import appsSlice from "./appsSlice";

export default configureStore({
  reducer: {
    apps: appsSlice
  }
})
