import { createSlice } from '@reduxjs/toolkit';

export const appSlice = createSlice({
  name: 'apps',
  initialState: {
    totalCart: 0.00,
    cart: []
  },
  reducers: {
    addToCart(state, action) {
      state.cart = [...state.cart, action.payload];
    },
    addNbProduct(state, action) {
      state.cart[action.payload].nbProduct++;
    },
    minusNbProduct(state, action) {
      state.cart[action.payload].nbProduct--;
    },
    deleteProduct(state, action) {
      state.cart.splice(action.payload, 1)
    },
    setTotal(state, action) {
      state.totalCart = action.payload;
    }
  }
})

export const { addToCart, addNbProduct, minusNbProduct, deleteProduct, setTotal } = appSlice.actions

export default appSlice.reducer
