import { useNavigate } from 'react-router-dom';
import maleUser from '../../../assets/maleUser.png';
import { useAuth } from '../../auth/authState';
import ReturnTitle from '../../ReturnTitle/ReturnTitle';
import './EditProfile.css';

function EditProfile() {
  const auth = useAuth();
  const navigate = useNavigate();

  const formSubmit = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    const formData = {
      firstName: data.get('first-name'),
      phoneNum: data.get('phone-num'),
      email: data.get('email'),
      password: data.get('password'),
      address: data.get('address'),
      city: data.get('city'),
      postalCode: data.get('postal-code'),
      idUser: auth.user.id
    }

    auth.update(formData, () => {
      navigate('/nav/home')
    })
  }

  return (
    <div className='profilePage mainPadding'>
      <ReturnTitle return={true} title='Modifier le profil' className='mb-32' />
      <div className='profileAvatar mb-32'>
        <img src={maleUser} alt='profile avatar' />
      </div>

      <div className='editProfileDown'>
        <form className='inputGroup mb-32' onSubmit={formSubmit}>
          <label className='inputText mb-24'>
            <input type='text' autoComplete='given-name' placeholder='Prénoms' id='first-name' name='first-name' required />
          </label>
          <label className='inputText mb-24'>
            <input type='tel' autoComplete='tel' placeholder='Numéro de téléphone' id='phone-num' name='phone-num' required />
          </label>
          <label className='inputText mb-24'>
            <input type='email' autoComplete='email' placeholder='Adresse email' id='email' name='email' required />
          </label>
          <label className='inputText mb-24'>
            <input type='text' autoComplete='street-address' placeholder='Adresse' id='address' name='address' required />
          </label>
          <label className='inputText mb-24'>
            <input type='text' autoComplete='address-level2' placeholder='Ville' id='city' name='city' required />
          </label>
          <label className='inputText mb-24'>
            <input type='text' autoComplete='postal-code' placeholder='Code postale' id='postal-code' name='postal-code' required />
          </label>
          <label className='inputText mb-32'>
            <input type='password' autoComplete='new-password' placeholder='Mot de passe' id='password' name='password' required />
          </label>
          <div className='buttonGroup mb-40'>
            <button type='submit' className='buttonMain'>Enregistrer</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditProfile;
