import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { useAuth } from '../../auth/authState';
import ReturnTitle from '../../ReturnTitle/ReturnTitle';
import './BillingAccept.css';
import { ReactComponent as Mastercard } from '../../../assets/mastercard.svg';

function BillingAccept() {
  const { totalCart } = useSelector(state => state.apps);
  const auth = useAuth();
  const navigate = useNavigate();
  const { state } = useLocation();
  const [order, setOrder] = useState(undefined);

  useEffect(() => {
    // GET 
    fetch(`/orders/order/user/${auth.user.id}/${state.orderId}`)
      .then(res => res.json())
      .then(res => setOrder(res[0]))
      .catch(error => console.log('error on get order:', error))
  }, [auth.user.id, state.orderId])

  return (
    <div className='billingAcceptPage'>
      <ReturnTitle return={false} title='Détail de la commande' className='mb-24' />
      <div className='orderGroup mb-24'>
        <div className='orderItem'>
          <div className='orderInfo'>
            <h4>N° de commande #{order && order.id}</h4>
            <span className='orderChips waiting'>
              En attente
            </span>
          </div>
          <h5>Livraison prévue le {order && order.date.slice(0, 9) + (parseInt(order.date[9]) + 3)}</h5>
        </div>
      </div>
      <div className='orderRecap'>
        <div className='orderListItem'>
          <h3>Votre commande</h3>
          <div>
            {order && order.items.map(item => (
              <div key={item.id} className='itemOrder'>
                <div className='leftSide'>
                  <span>{item.nb_product}</span>
                  <p>{item.name}</p>
                </div>
                <span>{Math.round((item.price * item.nb_product) * 100) / 100} €</span>
              </div>
            ))}
          </div>
        </div>
        <div className='orderDetails'>
          <div>
            <p>Total articles</p>
            <span>{totalCart} €</span>
          </div>
          <div>
            <p>Livraison</p>
            <span>2.99 €</span>
          </div>
          <div className='total'>
            <p>Total</p>
            <span>{Math.round((totalCart + 2.99) * 100) / 100} €</span>
          </div>
          <div>
            <p>Méthode de paiement</p>
            <div className='cardNum'>
              <Mastercard />
              <div className='crypt'>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
              </div>
              <div>
                <p>{state.credit.card_num[12]}</p>
                <p>{state.credit.card_num[13]}</p>
                <p>{state.credit.card_num[14]}</p>
                <p>{state.credit.card_num[15]}</p>
              </div>
            </div>
          </div>
        </div>
        <div className='buttonGroup mb-40'>
          <button onClick={() => navigate('/nav/home')} className='buttonMain'>Accueil</button>
        </div>
      </div>
    </div>
  )
}

export default BillingAccept;
