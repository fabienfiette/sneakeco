import { useLocation } from "react-router-dom";
import ReturnTitle from "../../ReturnTitle/ReturnTitle";
import './Product.css';
import { ReactComponent as AddIcon } from '../../../assets/addIcon.svg';
import { ReactComponent as MinusIcon } from '../../../assets/minusIcon.svg';
import { useDispatch } from 'react-redux';
import { addToCart } from '../../../store/appsSlice';
import { useState } from "react";

function Product() {
  const dispatch = useDispatch();
  const { state } = useLocation();
  const { product } = state
  const [nbProduct, setNbProduct] = useState(1);

  return (
    <div className="productPage">
      <div className="productTop">
        <ReturnTitle return={true} />
        <div className="imgCont">
          <img src={product.image} alt="Product" />
        </div>
        <div className="selectNumber">
          <div>
            <button onClick={() => setNbProduct(nbProduct + 1)}><AddIcon /></button>
            <span>{nbProduct}</span>
            <button onClick={() => nbProduct > 1 && setNbProduct(nbProduct - 1)}><MinusIcon /></button>
          </div>
        </div>
      </div>
      <div className="productInfo">
        <div className="productTitle mb-32">
          <h3>{product.name}</h3>
          <h3 className="paragTitle">{product.price} €</h3>
        </div>
        <p className="underTextMedium mb-60">
          {product.description}
        </p>
        <div className="buttonGroup">
          <button onClick={() => dispatch(addToCart({ nbProduct, ...product }))} className="buttonMain">Ajouter au panier</button>
        </div>
      </div>
    </div>
  )
}

export default Product;
