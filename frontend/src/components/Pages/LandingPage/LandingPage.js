import './LandingPage.css';
import logo from '../../../assets/sneakeco-white.png'

function LandingPage() {
  return (
    <div className='landingPage'>
      <img className='logo-large' src={logo} alt='Logo app' />
    </div>
  );
}

export default LandingPage;
