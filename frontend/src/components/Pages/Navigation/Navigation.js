import { Routes, Route, Navigate, useNavigate, useLocation } from 'react-router-dom';
import { ReactComponent as ShoesIcon } from '../../../assets/shoes.svg';
import { ReactComponent as FavIcon } from '../../../assets/fav.svg';
import { ReactComponent as HomeIcon } from '../../../assets/home.svg';
import { ReactComponent as CartIcon } from '../../../assets/bag.svg';
import { useEffect, useState } from 'react';
import Home from './Home/Home';
import AboutUs from './AboutUs/AboutUs';
import Cart from './Cart/Cart';
import RequireAuth from '../../auth/RequireAuth';

const navList = [
  {
    Icon: HomeIcon,
    title: 'home'
  },
  {
    Icon: ShoesIcon,
    title: 'aboutus'
  },
  {
    Icon: FavIcon,
    title: 'favorite'
  },
  {
    Icon: CartIcon,
    title: 'cart'
  }
]

function Navigation() {
  const location = useLocation();
  const navigate = useNavigate();
  const [navIndex, setNavIndex] = useState(0);

  useEffect(() => {
    setNavIndex(navList.findIndex(obj => obj.title === location.pathname.split('/')[2]));
  }, [location.pathname])

  return (
    <>
      <Routes>
        <Route path='/home' element={<Home />} />
        <Route path='/aboutus/*' element={<AboutUs />} />
        <Route path='/cart' element={<RequireAuth><Cart /></RequireAuth>} />
        <Route path='/*' element={<Navigate to='/' />} />
      </Routes>
      <nav className='navbar'>
        {navList.map((item, index) => (
          <button className={`navIcon ${navIndex === index && 'active'}`} onClick={() => { setNavIndex(index); navigate(item.title) }} key={index}>
            <item.Icon />
          </button>
        ))}
      </nav>
    </>
  )
}

export default Navigation;
