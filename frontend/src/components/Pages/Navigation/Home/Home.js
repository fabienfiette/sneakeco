import { ReactComponent as AddIcon } from '../../../../assets/addIcon.svg';
import maleUser from '../../../../assets/maleUser.png';
// import femaleUser from '../../../../assets/femaleUser.png';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import defaultUser from '../../../../assets/defaultUser.png';
import { ReactComponent as PosIcon } from '../../../../assets/positionIcon.svg';
import { useAuth } from '../../../auth/authState';
import './Home.css';

function Home() {
  const auth = useAuth();
  const [chipsIndex, setChipsIndex] = useState(1);
  const [selectCate, setSelectCate] = useState('Tout');
  const [categories, setCategories] = useState(undefined);
  const [products, setProducts] = useState(undefined);

  useEffect(() => {
    // Categories
    fetch(`/categories/`)
      .then(res => res.json())
      .then(res => setCategories(res))
      .catch(error => console.log('error on categories:', error));
    // Poducts
    fetch(`/products/`)
      .then(res => res.json())
      .then(res => setProducts(res))
      .catch(error => console.log('error on products:', error));
  }, [])

  return (
    <div className='mainPadding homePage'>
      <section className='mb-32'>
        <div className='userInfo'>
          <h3 className='paragTitle'>Bonjour {auth.user && auth.user.first_name}</h3>
          <div>
            {auth.user && (
              <>
                <PosIcon />
                <span>{auth.user.postal_code} {auth.user.adress}, {auth.user.city}</span>
              </>
            )}
          </div>
        </div>
        {auth.user ? (
          <Link to='/profile' className='profilePicture'>
            <img src={maleUser} alt='User' />
          </Link>
        ) : (
          <Link to='/sign/up' className='profilePicture'>
            <img src={defaultUser} alt='User' />
          </Link>
        )}
      </section>

      <div className='chipsGroup mb-32'>
        {categories && categories.map((item) =>
          <button className={`chipsButton ${chipsIndex === item.id && 'active'}`} onClick={() => { setChipsIndex(item.id); setSelectCate(item.name) }} key={item.id}>{item.name}</button>
        )}
      </div>

      <div className='shopItemList mb-32'>
        {products && products.filter(obj => obj.cat_name === selectCate || selectCate === 'Tout').map((item) => (
          <Link to={`/product/${item.id}`} state={{ product: item }} className='shopItem' key={item.id}>
            <img src={item.image} alt='Kit' />
            <div>
              <h6>{item.name}</h6>
              <div>
                <span>{item.price} €</span>
                <button>
                  <AddIcon />
                </button>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default Home;
