import ReturnTitle from '../../../ReturnTitle/ReturnTitle';
import './Cart.css';
import { ReactComponent as AddIcon } from '../../../../assets/addIcon.svg';
import { ReactComponent as MinusIcon } from '../../../../assets/minusIcon.svg';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addNbProduct, minusNbProduct, deleteProduct, setTotal } from '../../../../store/appsSlice';
import { useEffect } from 'react';

function Cart() {
  const { cart, totalCart } = useSelector(state => state.apps);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    let newTotal = 0;
    cart.forEach(item => {
      newTotal = newTotal + (item.price * item.nbProduct);
    });
    dispatch(setTotal(Math.round(newTotal * 100) / 100))
  }, [cart, dispatch])

  return (
    <div className='cartPage mainPadding'>
      <ReturnTitle return={false} title='Votre Panier' className='mb-60' />
      {/* <div className='reducBanner mb-40'>
        <div>
          <h3 className='aboutTitle'>30% OFF</h3>
          <p className='underTextMedium'>Utilisez le code <span className='fontBold'>87412</span> lors du paiement</p>
        </div>
      </div> */}

      <div className='cartContainer mb-10'>
        {cart && cart.map((item, index) => (
          <div key={index} className='cartItem mb-40'>
            <div className='ciContent'>
              <div className='ciImg'>
                <img src={item.image} alt='product' />
              </div>
              <div className='ciInfo'>
                <div>
                  <h6>{item.name}</h6>
                  <p>{item.name} description</p>
                </div>
                <span>{item.price}€</span>
              </div>
            </div>
            <div className='ciAction'>
              <button onClick={() => dispatch(addNbProduct(index))}><AddIcon /></button>
              <span>{item.nbProduct}</span>
              <button onClick={() => item.nbProduct > 1 ? dispatch(minusNbProduct(index)) : dispatch(deleteProduct(index))}><MinusIcon /></button>
            </div>
          </div>
        ))}
      </div>

      <div className='cartTotal'>
        <h6>Total</h6>
        <span>{totalCart}€</span>
      </div>

      <div className='buttonGroup'>
        <button onClick={() => navigate('/billing')} className='buttonMain'>Paiement</button>
      </div>
    </div >
  )
}

export default Cart;
