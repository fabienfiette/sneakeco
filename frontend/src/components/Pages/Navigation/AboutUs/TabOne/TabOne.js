import { ReactComponent as EiffelTower } from '../../../../../assets/eiffelTower.svg';
import { ReactComponent as EcoIcon } from '../../../../../assets/eco.svg';
import { ReactComponent as RecycleIcon } from '../../../../../assets/recycle.svg';
import { ReactComponent as AproovedIcon } from '../../../../../assets/aprooved.svg';

function TabOne() {
  return (
    <>
      <div className="mb-60">
        <h4 className="aboutTitle mb-16">Des produits de qualité</h4>
        <p className="aboutSecondary">À une époque où la sensibilisation et le recyclage gagnent du terrain, il n'y a pas de véritable solution pour un marché en plein essor comme celui des baskets. C’est la que Sneakeco entre en jeu avec une gamme de <span className="activeText">produits entierement made in France</span>, <span className="activeText">éco-responsable</span> ainsi que des recharges afin <span className="activeText">d’éviter le gaspillage</span>.</p>
      </div>

      <div className="aboutDesc mb-60">
        <div>
          <EiffelTower />
          <h4 className="aboutTitle">Made in France</h4>
          <p className="aboutSecondary">Retrouver le savoir faire Français, gage de qualité et de fiabilité. Tous nos entrepôts se trouvent en France.</p>
        </div>
        <div>
          <EcoIcon />
          <h4 className="aboutTitle">Éco-responsable</h4>
          <p className="aboutSecondary">Tous nos produits sont conçus dans le respect de l’environnement. Nous n’utilisons pas de plastique ni de matière polluente.</p>
        </div>
        <div>
          <RecycleIcon />
          <h4 className="aboutTitle">Réutilisable</h4>
          <p className="aboutSecondary">Grâce à notre système de recharges, plus besions de jeter le spray une fois fini. Il suffit de commander des recharges et c’est repartie.</p>
        </div>
        <div>
          <AproovedIcon />
          <h4 className="aboutTitle">{'Testé & approuvé'}</h4>
          <p className="aboutSecondary">Tous les clients ayant acheté nos produits sont satisfait. En effet, nous mettons un point d’honneur sur la qualité de nos produits.</p>
        </div>
      </div>
    </>
  );
}

export default TabOne;
