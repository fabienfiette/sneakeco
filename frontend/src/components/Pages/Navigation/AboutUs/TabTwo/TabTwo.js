import { useEffect, useState } from 'react';
import demo2 from '../../../../../assets/demo2.png';

function TabTwo() {
  const [activeImg, setActiveImg] = useState(0);

  useEffect(() => {
    const eleBox = document.getElementById('imageCompareBox');

    eleBox.scrollTo(342 * activeImg, 0)
  }, [activeImg])

  return (
    <>
      <div className="mb-32">
        <h4 className="aboutTitle mb-16">Un résultat impressionnant</h4>
        <p className="aboutSecondary mb-16">Sneakeco vous permet de retrouver vos paires tel que vous les avez acheté le tout dans le respect de l’environnement. Notre kit de nétoyage contient une brosse en bois, un chiffon microfibre de bambou ainsi qu’un spray nettoyant.</p>
        <p className="aboutSecondary">Voici quelques exemples de paires remises à neuf grâce à nous !</p>
      </div>

      <div className="imageCompare mb-24" id='imageCompareBox'>
        <img src={demo2} alt='Démonstration' />
        <img src={demo2} alt='Démonstration' />
        <img src={demo2} alt='Démonstration' />
      </div>

      <div className="divider mb-60" id='divider'>
        <span onClick={() => setActiveImg(0)} className={activeImg === 0 ? 'active' : ''}></span>
        <span onClick={() => setActiveImg(1)} className={activeImg === 1 ? 'active' : ''}></span>
        <span onClick={() => setActiveImg(2)} className={activeImg === 2 ? 'active' : ''}></span>
      </div>
    </>
  );
}

export default TabTwo;
