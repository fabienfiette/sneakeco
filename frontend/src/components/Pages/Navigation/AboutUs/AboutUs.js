import { useEffect, useState } from 'react';
import { Navigate, Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import ReturnTitle from '../../../ReturnTitle/ReturnTitle';
import './AboutUs.css';
import TabOne from './TabOne/TabOne';
import TabTwo from './TabTwo/TabTwo';

function AboutUs() {
  const location = useLocation();
  const navigate = useNavigate();
  const [activeTab, setActiveTab] = useState('one');

  useEffect(() => {
    setActiveTab(location.pathname.split('/')[3]);
  }, [location.pathname])

  return (
    <div className='aboutUsPage mainPadding'>
      <ReturnTitle return={false} title='Sneakeco' className='mb-32' />
      <section className='aboutTab'>
        <h3 className={activeTab === 'one' ? 'active' : ''} onClick={() => { setActiveTab('one'); navigate('one') }}>{'Nos valeurs'}</h3>
        <h3 className={activeTab === 'two' ? 'active' : ''} onClick={() => { setActiveTab('two'); navigate('two') }}>{'Avant & Après'}</h3>
      </section>
      <Routes>
        <Route path='/one' element={<TabOne />} />
        <Route path='/two' element={<TabTwo />} />
        <Route path='/*' element={<Navigate to='/nav/aboutus/one' />} />
      </Routes>
    </div>
  );
}

export default AboutUs;
