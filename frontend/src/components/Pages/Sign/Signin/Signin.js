import { Link, useNavigate } from 'react-router-dom';
import googleIcon from '../../../../assets/googleIcon.svg'
import facebookIcon from '../../../../assets/facebookIcon.svg'
import { useAuth } from '../../../auth/authState';
import { useEffect } from 'react';

function Signin() {
  const auth = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (auth.user) {
      navigate("/nav/home")
    }
  })

  const formSubmit = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    const formData = {
      email: data.get('email'),
      password: data.get('password'),
    }

    auth.signin(formData)
  }

  return (
    <form className='signForm' onSubmit={formSubmit}>
      <div className='inputGroup'>
        <label className='inputText mb-32'>
          <input type='email' autoComplete='email' placeholder='Adresse email' id='email' name='email' required />
        </label>
        <label className='inputText mb-32'>
          <input type='password' autoComplete='current-password' placeholder='Mot de passe' id='password' name='password' required />
        </label>
        <label className='inputCheckbox mb-32'>
          <input type='checkbox' />
          Se souvenir de moi
        </label>
      </div>
      <div className='buttonGroup'>
        <button type='submit' className='buttonMain mb-32'>Connexion</button>
        <Link to='/forgetPass' className='linkText mb-32'>Mot de passe oublié ?</Link>
        <hr className='mb-32' />
        <div className='socialSignButtonGroup mb-32'>
          <button className='socialSignButton mr-16'><img src={googleIcon} alt='Google icon' />Google</button>
          <button className='socialSignButton'><img src={facebookIcon} alt='Facebook icon' />Facebook</button>
        </div>
        <span className='underTextLight'>Vous n’avez pas de compte ? <Link to='/sign/up' className='linkText'>Inscrivez-vous</Link></span>
      </div>
    </form>
  );
}

export default Signin;
