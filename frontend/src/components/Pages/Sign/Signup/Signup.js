import { useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import facebookIcon from '../../../../assets/facebookIcon.svg';
import googleIcon from '../../../../assets/googleIcon.svg';
import infoCircle from '../../../../assets/infoCircle.svg';
import { useAuth } from '../../../auth/authState';

function Signup() {
  const auth = useAuth();
  const navigate = useNavigate();
  const location = useLocation();

  const from = location.state?.from?.pathname || '/'

  useEffect(() => {
    if (auth.user) navigate(from)
  }, [auth.user, from, navigate])

  const formSubmit = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    const formData = {
      firstName: data.get('first-name'),
      phoneNum: data.get('phone-num'),
      email: data.get('email'),
      password: data.get('password'),
      address: data.get('address'),
      city: data.get('city'),
      postalCode: data.get('postal-code')
    }

    auth.signup(formData, () => {
      navigate('/sign/in')
    })
  }

  return (
    <form className='signForm' onSubmit={formSubmit}>
      <div className='inputGroup'>
        <label className='inputText mb-24'>
          <input type='text' autoComplete='given-name' placeholder='Prénoms' id='first-name' name='first-name' required />
        </label>
        <label className='inputText mb-24'>
          <input type='tel' autoComplete='tel' placeholder='Numéro de téléphone' id='phone-num' name='phone-num' required />
        </label>
        <label className='inputText mb-24'>
          <input type='email' autoComplete='email' placeholder='Adresse email' id='email' name='email' required />
        </label>
        <label className='inputText mb-24'>
          <input type='text' autoComplete='street-address' placeholder='Adresse' id='address' name='address' required />
        </label>
        <label className='inputText mb-24'>
          <input type='text' autoComplete='address-level2' placeholder='Ville' id='city' name='city' required />
        </label>
        <label className='inputText mb-24'>
          <input type='text' autoComplete='postal-code' placeholder='Code postale' id='postal-code' name='postal-code' required />
        </label>
        <label className='inputText mb-16'>
          <input type='text' autoComplete='new-password' placeholder='Mot de passe' id='password' name='password' required />
        </label>
        <p className='passwordWarning mb-32'>
          <img src={infoCircle} className='mr-8' width='16px' height='16px' alt='Info circle' />
          <span>Minimum 6 caractères, dont au moins un chiffre, et un caractère spécial</span>
        </p>
        <label className='inputText mb-24'>
          <input type='password' autoComplete='new-password' placeholder='Confirmé le mot de passe' id='validPassword' name='validPassword' required />
        </label>
      </div>
      <div className='buttonGroup'>
        <button type='submit' className='buttonMain mb-24'>S'inscrire</button>
        <hr className='mb-24' />
        <div className='socialSignButtonGroup mb-24'>
          <button className='socialSignButton mr-16'><img src={googleIcon} alt='Google icon' />Google</button>
          <button className='socialSignButton'><img src={facebookIcon} alt='Facebook icon' />Facebook</button>
        </div>
        <span className='underTextLight'>Vous avez déjà un compte ? <Link to='/sign/in' className='linkText'>Se connecter</Link></span>
      </div>
    </form>
  );
}

export default Signup;
