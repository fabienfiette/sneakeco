import './Sign.css'
import defaultUser from '../../../assets/defaultUser.png';
import { Navigate, Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import SigninPage from './Signin/Signin';
import SignupPage from './Signup/Signup';
import { useEffect, useState } from 'react';

function Sign() {
  const [activeTab, setActiveTab] = useState('signin');
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    setActiveTab(location.pathname.split('/').join(''))
  }, [location.pathname])

  return (
    <div className="signPage">
      <section className='mb-32'>
        <div className='mb-32'>
          <div className='signTab'>
            <button onClick={() => { setActiveTab('signin'); navigate('in') }} className={activeTab === 'signin' ? 'active' : ''}>Connexion</button>
            <button onClick={() => { setActiveTab('signup'); navigate('up') }} className={activeTab === 'signup' ? 'active' : ''}>Inscription</button>
          </div>
          <div className='profilePicture'>
            <img src={defaultUser} alt='User' />
          </div>
        </div>
        <div>
          <h3 className='paragTitle mb-10'>Bonjour,</h3>
          <span className='underTextMedium'>
            Entrez vos informations ci-dessous ou avec un compte social
          </span>
        </div>
      </section>
      <Routes>
        <Route path='/in' element={<SigninPage />} />
        <Route path='/up' element={<SignupPage />} />
        <Route path='/*' element={<Navigate to='/' />} />
      </Routes>
    </div>
  );
}

export default Sign;
