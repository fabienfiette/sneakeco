import { useNavigate } from 'react-router-dom';
import { ReactComponent as EditIcon } from '../../../assets/editIcon.svg';
import maleUser from '../../../assets/maleUser.png';
import { useAuth } from '../../auth/authState';
import ReturnTitle from '../../ReturnTitle/ReturnTitle';
import './Profile.css';

function Profile() {
  const auth = useAuth();
  const navigate = useNavigate();

  return (
    <div className='profilePage mainPadding'>
      <ReturnTitle return={true} title='Mon profil' className='mb-32' />
      <div className='profileCard mb-32'>
        <div className='info'>
          <img src={maleUser} alt='profile avatar' />
          <div className='infoText'>
            <p>{auth.user.first_name}</p>
            <span>{auth.user.phone_num}</span>
          </div>
        </div>
        <button onClick={() => navigate('/editProfile')}>
          <EditIcon />
        </button>
      </div>

      <div className='editProfileDown'>
        <button onClick={() => auth.signout(() => navigate('/nav/home'))} className='disconnect'>Se déconnecter</button>
      </div>
    </div>
  );
}

export default Profile;
