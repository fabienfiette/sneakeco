import { useNavigate } from 'react-router-dom';
import { ReactComponent as Mastercard } from '../../../assets/mastercard.svg';
import { useAuth } from '../../auth/authState';
import ReturnTitle from '../../ReturnTitle/ReturnTitle';
import './AddCreditCard.css';

function AddCreditCard() {
  const auth = useAuth();
  const navigate = useNavigate();

  const submitForm = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    const formData = {
      cardNum: data.get('card-num'),
      expiration: data.get('expiration'),
      name: data.get('name'),
      cvcCode: data.get('cvc-code'),
      userId: auth.user.id
    }

    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(formData)
    };

    // Post
    fetch(`/credit_cards/`, requestOptions)
      .then(res => res.json())
      .then(() => navigate('/billing', { replace: true }))
      .catch(error => console.log('error on addCreditCard:', error));
  }

  return (
    <div className='addCreditCardPage mainPadding'>
      <ReturnTitle return={true} title='Ajouter une carte' className='mb-24' />
      <form className='cardInfo mb-40' onSubmit={submitForm}>
        <div className='creditCard'>
          <Mastercard />
          <h3>HOLDER NAME</h3>
          <h4>0000 0000 0000 0000</h4>
        </div>

        <div className='inputGroup'>
          <label className='inputText'>
            <input type='text' autoComplete='cc-number' placeholder='Numéro de carte' id='card-num' name='card-num' required />
          </label>
          <label className='inputText'>
            <input type='text' autoComplete='cc-name' placeholder='Nom et prénom' id='name' name='name' required />
          </label>
          <div>
            <label className='inputText'>
              <input type='text' autoComplete='cc-exp' placeholder='Expire le' id='expiration' name='expiration' required />
            </label>
            <label className='inputText'>
              <input type='text' autoComplete='cc-csc' placeholder='CVC' id='cvc-code' name='cvc-code' required />
            </label>
          </div>
        </div>

        <div className='buttonGroup'>
          <button type='submit' className='buttonMain'>Enregistrer</button>
        </div>
      </form>
    </div >
  )
}

export default AddCreditCard;
