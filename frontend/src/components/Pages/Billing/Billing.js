import ReturnTitle from '../../ReturnTitle/ReturnTitle';
import './Billing.css';
import { ReactComponent as PosIcon } from '../../../assets/positionIcon.svg';
import { ReactComponent as TimeIcon } from '../../../assets/timeIcon.svg';
import { ReactComponent as AddIcon } from '../../../assets/addIcon.svg';
import { ReactComponent as Mastercard } from '../../../assets/mastercard.svg';
import { ReactComponent as Paypal } from '../../../assets/paypal.svg';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../auth/authState';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

function Billing() {
  const { cart, totalCart } = useSelector(state => state.apps);
  const auth = useAuth();
  const navigate = useNavigate();
  const [creditCards, setCreditCards] = useState(undefined);

  useEffect(() => {
    // GET 
    fetch(`/credit_cards/credit_card/user/${auth.user.id}`)
      .then(res => res.json())
      .then(res => setCreditCards(res))
      .catch(error => console.log('error on get credit card of user:', error))
  }, [auth.user.id])

  const submitPayment = (e) => {
    e.preventDefault();

    const formData = {
      date: new Date().toISOString().slice(0, 10),
      delivered: false,
      userId: auth.user.id,
      cart
    }

    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(formData)
    };

    // Post
    fetch(`/orders/`, requestOptions)
      .then(res => res.json())
      .then(res => navigate('/paymentAccept', { state: { orderId: res.insertId, credit: creditCards[0] } }))
      .catch(error => console.log('error on addCreditCard:', error));
  }

  return (
    <div className='billingPage mainPadding'>
      <ReturnTitle return={true} title='Paiement' className='mb-24' />
      <div className='address mb-32'>
        <div className='addrInfo'>
          <PosIcon />
          <p>{auth.user.postal_code} {auth.user.adress}, {auth.user.city}</p>
        </div>
        <div className='addrDate'>
          <TimeIcon />
          <p>3 - 4 jours</p>
        </div>
      </div>

      <div className='payment'>
        <div className='paymentMethod'>
          <h4>Paiement</h4>

          <label htmlFor='creditCard' className='options'>
            <div className='method'>
              <div className='iconBox'>
                <Mastercard />
              </div>
              <p>Carte de crédit</p>
            </div>
            <div className='radioGroup'>
              <input type='radio' name='paiment' id='creditCard' />
            </div>
          </label>
          <label htmlFor='paypal' className='options'>
            <div className='method'>
              <div className='iconBox'>
                <Paypal />
              </div>
              <p>Paypal</p>
            </div>
            <div className='radioGroup'>
              <input type='radio' name='paiment' id='paypal' />
            </div>
          </label>

        </div>
        <div className='cards'>
          <button onClick={() => navigate('/addCreditCard')} className='buttonMain'><AddIcon /></button>
          {creditCards && creditCards.map(item => (
            <div key={item.id} className='creditCard'>
              <Mastercard />
              <div className='cardNum'>
                <div className='crypt'>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                </div>
                <div>
                  <p>{item.card_num[12]}</p>
                  <p>{item.card_num[13]}</p>
                  <p>{item.card_num[14]}</p>
                  <p>{item.card_num[15]}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
        {/* <div className='input'>
          <h4 className='mb-10'>Code promotion</h4>
          <input type='text' placeholder='Entrez votre code'/>
        </div> */}
        <div className='summary'>
          <div className='billingInfo'>
            <div>
              <p>Total articles</p>
              <span>{totalCart} €</span>
            </div>
            <div>
              <p>Livraison</p>
              <span>2.99 €</span>
            </div>
          </div>
          <div className='billingTotal'>
            <p>Total</p>
            <span>{Math.round((totalCart + 2.99) * 100) / 100} €</span>
          </div>
        </div>
        <div className='buttonGroup'>
          <button onClick={submitPayment} className='buttonMain'>Payer</button>
        </div>
      </div>
    </div>
  )
}

export default Billing;
