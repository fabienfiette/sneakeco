import { useNavigate } from "react-router-dom";
import ReturnTitle from "../../ReturnTitle/ReturnTitle";
import infoCircle from '../../../assets/infoCircle.svg'

function ResetPass() {
  const navigate = useNavigate();

  return (
    <div className='mainPadding'>
      <ReturnTitle return={true} className='mb-32' />
      <h3 className='paragTitle mb-10'>Réinitialiser le mot de passe</h3>
      <span className='underTextLight mb-32'>Entrez votre nouveau mot de passe</span>
      <div className='inputGroup'>
        <label className='inputText mb-16'>
          <input type='password' autoComplete='new-password' placeholder='Mot de passe' />
        </label>
        <p className='passwordWarning mb-32'>
          <img src={infoCircle} className='mr-8' width='16px' height='16px' alt='Info circle' />
          <span>Minimum 6 caractères, dont au moins un chiffre, et un caractère spécial</span>
        </p>
        <label className='inputText mb-24'>
          <input type='password' autoComplete='new-password' placeholder='Confirmé le mot de passe' />
        </label>
      </div>
      <div className='buttonGroup'>
        <button onClick={() => navigate('/passSuccess')} className='buttonMain'>Changer</button>
      </div>
    </div>
  );
}

export default ResetPass;
