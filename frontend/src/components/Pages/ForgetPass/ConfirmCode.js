import { useState } from "react";
import { useNavigate } from "react-router-dom";
import ReturnTitle from "../../ReturnTitle/ReturnTitle";
import './ConfirmCode.css'

function ConfirmCode() {
  const [confirmCode, setConfirmCode] = useState('');
  const navigate = useNavigate();

  const handleInput = (e) => {
    if (!(e.target.value.length > 4) && !isNaN(e.target.value)) {
      setConfirmCode(e.target.value)
    }
  }

  return (
    <div className='mainPadding'>
      <ReturnTitle return={true} className='mb-32' />
      <h3 className='paragTitle mb-10'>Entrez votre code</h3>
      <span className='underTextLight mb-32'>Entrez le code que nous venons de vous envoyer</span>
      <label className='confirmCodeLabel mb-16'>
        <div className='confirmCodeCase'>{confirmCode[0] ? confirmCode[0] : '_'}</div>
        <div className='confirmCodeCase'>{confirmCode[1] ? confirmCode[1] : '_'}</div>
        <div className='confirmCodeCase'>{confirmCode[2] ? confirmCode[2] : '_'}</div>
        <div className='confirmCodeCase'>{confirmCode[3] ? confirmCode[3] : '_'}</div>
        <input type='text' value={confirmCode} onChange={handleInput} />
      </label>
      <div className='flexEndText mb-32'>
        <button className='linkText'>Renvoyer le code</button>
      </div>
      <div className='buttonGroup'>
        <button onClick={() => navigate('/resetPass')} className='buttonMain'>Suivant</button>
      </div>
    </div>
  );
}

export default ConfirmCode;
