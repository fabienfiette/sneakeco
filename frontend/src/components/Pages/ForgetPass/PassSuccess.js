import { useNavigate } from "react-router-dom";
import checkIcon from '../../../assets/checkOutlineIcon.svg'

function PassSuccess() {
  const navigate = useNavigate();

  return (
    <div className='mainPadding passSuccess'>
      <h3 className='paragTitle mb-32' style={{ marginTop: '91px' }}>Changer son mot de passe</h3>
      <img src={checkIcon} className='mb-32' alt='Check'/>
      <span className='underTextMedium mb-32'>Votre mot de passe à correctement été changé !</span>
      <div className='buttonGroup'>
        <button onClick={() => navigate('/sign/in')} className='buttonMain'>Se connecter</button>
      </div>
    </div>
  );
}

export default PassSuccess;
