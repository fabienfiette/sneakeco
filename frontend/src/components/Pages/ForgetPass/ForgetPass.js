import { useNavigate } from "react-router-dom";
import ReturnTitle from "../../ReturnTitle/ReturnTitle";

function ForgetPass() {
  const navigate = useNavigate();

  return (
    <div className='mainPadding'>
      <ReturnTitle return={true} className='mb-32' />
      <h3 className='paragTitle mb-10'>Mot de passe oublié ?</h3>
      <span className='underTextLight mb-32'>Saisissez le numéro de téléphone associé à votre compte et nous vous enverrons un code pour réinitialiser votre mot de passe.</span>
      <div className='inputGroup'>
        <label className='inputText mb-32'>
          <input type='email' autoComplete='email' placeholder='Adresse email' />
        </label>
      </div>
      <div className='buttonGroup'>
        <button onClick={() => navigate('/confirmCode')} className='buttonMain'>Envoyer</button>
      </div>
    </div>
  );
}

export default ForgetPass;
