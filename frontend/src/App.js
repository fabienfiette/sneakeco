import { Navigate, Route, Routes } from 'react-router-dom';
import { AuthProvider } from './components/auth/authState';
import RequireAuth from './components/auth/RequireAuth';
import AddCreditCard from './components/Pages/AddCreditCard/AddCreditCard';
import Billing from './components/Pages/Billing/Billing';
import BillingAccept from './components/Pages/BillingAccept/BillingAccept';
import ConfirmCode from './components/Pages/ForgetPass/ConfirmCode';
import ForgetPass from './components/Pages/ForgetPass/ForgetPass';
import PassSuccess from './components/Pages/ForgetPass/PassSuccess';
import ResetPass from './components/Pages/ForgetPass/ResetPass';
import LandingPage from './components/Pages/LandingPage/LandingPage';
import Navigation from './components/Pages/Navigation/Navigation';
import Product from './components/Pages/Product/Product';
import Profile from './components/Pages/Profile/Profile';
import Sign from './components/Pages/Sign/Sign';
// Fonts
import './assets/fonts/ReadexPro/ReadexPro-VariableFont_wght.ttf';
// Styles
import './styles/reset.css';
import './styles/sneakeco.css';
import EditProfile from './components/Pages/EditProfile/EditProfile';

function App() {
  return (
    <AuthProvider>
      <Routes>
        <Route path='/landing' element={<LandingPage />} />
        <Route path='/sign/*' element={<Sign />} />
        <Route path='/forgetPass' element={<ForgetPass />} />
        <Route path='/confirmCode' element={<ConfirmCode />} />
        <Route path='/resetPass' element={<ResetPass />} />
        <Route path='/passSuccess' element={<PassSuccess />} />
        <Route path='/nav/*' element={<Navigation />} />
        <Route path='/profile' element={<RequireAuth><Profile /></RequireAuth>} />
        <Route path='/editProfile' element={<RequireAuth><EditProfile /></RequireAuth>} />
        <Route path='/addCreditCard' element={<RequireAuth><AddCreditCard /></RequireAuth>} />
        <Route path='/product/:id' element={<Product />} />
        <Route path='/billing' element={<RequireAuth><Billing /></RequireAuth>} />
        <Route path='/paymentAccept' element={<RequireAuth><BillingAccept /></RequireAuth>} />
        <Route path='*' element={<Navigate to="/nav/home" />} />
      </Routes>
    </AuthProvider>
  );
}

export default App;
