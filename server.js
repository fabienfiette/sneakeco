const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const port = process.env.PORT || 3001;

app.use(express.static(path.join(__dirname, '/frontend/build')))

const userRoute = require("./routes/users/index")
const productRoute = require("./routes/products/index")
const categorieRoute = require("./routes/categories/index")
const creditCardRoute = require("./routes/credit_cards/index")
const orderRoute = require("./routes/orders/index")

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/categories", categorieRoute);
app.use("/credit_cards", creditCardRoute);
app.use("/orders", orderRoute);

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'frontend/build/index.html'));
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
