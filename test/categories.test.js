const request = require('supertest');
const { getCategorie, putCategorie } = require('./mock/categories');
const { baseUrl } = require('./mock/globalVar');

describe(`${baseUrl}/categories/`, () => {
  describe(`GET categories/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/categories')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`POST categories/ =>`, () => {
    test("Insert a new categorie in table 'categories'", async () => {
      const res = await request(baseUrl)
        .post('/categories')
        .set('Accept', 'application/json')
        .send({
          "name": "Postman"
        });

      expect(res.statusCode).toBe(200);
      getCategorie[0].id = res.body.insertId;
      putCategorie[0].id = res.body.insertId;
    });
  });

  describe(`GET categories/categorie/:id =>`, () => {
    test(`Get a categorie with id`, async () => {
      const res = await request(baseUrl)
        .get(`/categories/categorie/${getCategorie[0].id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(getCategorie));
    });
  });

  describe(`PUT categories/update/:id =>`, () => {
    test(`Update categorie with specific body`, async () => {
      const res = await request(baseUrl)
        .put(`/categories/update/${getCategorie[0].id}`)
        .set('Accept', 'application/json')
        .send({
          "name": "Post"
        });

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putCategorie));
    });
  });

  describe(`DELETE categories/categorie/:id =>`, () => {
    test(`Delete a categorie with id`, async () => {
      const res = await request(baseUrl)
        .delete(`/categories/categorie/${getCategorie[0].id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putCategorie));
    });
  });
});
