const putProduct = [
  {
    "id": 0,
    "name": "name",
    "description": "description",
    "price": 3.99,
    "categorie_id": 1,
    "image": "https://i.imgur.com/twqB7v7.png"
  }
]

const getProduct = [
  {
    "id": 0,
    "name": "Product name",
    "description": "Product description",
    "price": 4.99,
    "categorie_id": 1,
    "image": null,
    "cat_name": "Tout"
  }
]

module.exports = {
  putProduct,
  getProduct
}
