const putCategorie = [
  {
    "id": 0,
    "name": "Post"
  }
]

const getCategorie = [
  {
    "id": 0,
    "name": "Postman"
  }
]

module.exports = {
  putCategorie,
  getCategorie
}
