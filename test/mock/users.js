const bcrypt = require("bcryptjs");
const salt = "$2a$10$5UTQoU4Fh6.7oHE7Qyn6Vu";

const putUser = [
  {
    "id": 0,
    "first_name": "ManPost",
    "phone_num": "0700000000",
    "email": "postman@email.com",
    "password": bcrypt.hashSync("pass", salt),
    "adress": "address",
    "city": "citi",
    "postal_code": "00000"
  }
]

const getUser = [
  {
    "id": 0,
    "first_name": "PostMan",
    "phone_num": "0600000000",
    "email": "postman@email.com",
    "password": bcrypt.hashSync("postmanPassword", salt),
    "adress": "address",
    "city": "city",
    "postal_code": "00000"
  }
]

module.exports = {
  putUser,
  getUser
}
