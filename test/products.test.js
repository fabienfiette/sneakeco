const request = require('supertest');
const { getProduct, putProduct } = require('./mock/products');
const { baseUrl } = require('./mock/globalVar');

describe(`${baseUrl}/products/`, () => {
  describe(`GET products/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/products')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`POST products/ =>`, () => {
    test("Insert a new product in table 'products'", async () => {
      const res = await request(baseUrl)
        .post('/products')
        .set('Accept', 'application/json')
        .send({
          "name": "Product name",
          "description": "Product description",
          "price": 4.99,
          "categorieId": 1
        });

      expect(res.statusCode).toBe(200);
      getProduct[0].id = res.body.insertId;
      putProduct[0].id = res.body.insertId;
    });
  });

  describe(`GET products/product/:id =>`, () => {
    test(`Get a product with id`, async () => {
      const res = await request(baseUrl)
        .get(`/products/product/${getProduct[0].id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(getProduct));
    });
  });

  describe(`PUT products/update/:id =>`, () => {
    test(`Update product with specific body`, async () => {
      const res = await request(baseUrl)
        .put(`/products/update/${getProduct[0].id}`)
        .set('Accept', 'application/json')
        .send({
          "name": "name",
          "description": "description",
          "price": 3.99,
          "categorieId": 1,
          "image": "https://i.imgur.com/twqB7v7.png"
        });

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putProduct));
    });
  });

  describe(`DELETE products/product/:id =>`, () => {
    test(`Delete a product with id`, async () => {
      const res = await request(baseUrl)
        .delete(`/products/product/${getProduct[0].id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putProduct));
    });
  });
});
