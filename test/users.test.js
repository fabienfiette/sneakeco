const request = require('supertest');
const { getUser, putUser } = require('./mock/users');
const { baseUrl } = require('./mock/globalVar');

let token = '';

describe(`${baseUrl}/users/`, () => {
  describe(`GET users/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/users')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`POST users/ =>`, () => {
    test("Insert a new user in table 'users'", async () => {
      const res = await request(baseUrl)
        .post('/users')
        .set('Accept', 'application/json')
        .send({
          "firstName": "PostMan",
          "phoneNum": "0600000000",
          "email": "postman@email.com",
          "password": "postmanPassword",
          "address": "address",
          "city": "city",
          "postalCode": "00000"
        });

      expect(res.statusCode).toBe(200);
      getUser[0].id = res.body.insertId;
      putUser[0].id = res.body.insertId;
    });
  });

  describe(`POST users/signin =>`, () => {
    test(`Verify avaible user with email and password`, async () => {
      const res = await request(baseUrl)
        .post(`/users/signin`)
        .set('Accept', 'application/json')
        .send({
          "email": "postman@email.com",
          "password": "postmanPassword"
        });

      expect(res.statusCode).toBe(200);
      token = res.body[0].token;
      expect(JSON.stringify(res.body)).toBe(JSON.stringify([{ ...getUser[0], token: res.body[0].token }]));
    });
  });

  describe(`GET users/user/:id =>`, () => {
    test(`Get a user with id`, async () => {
      const res = await request(baseUrl)
        .get(`/users/user/${getUser[0].id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(getUser));
    });
  });

  describe(`PUT users/update/:id =>`, () => {
    test(`Update user with specific body`, async () => {
      const res = await request(baseUrl)
        .put(`/users/update/${getUser[0].id}`)
        .set({ 'authorization': `Bearer ${token}`, 'Accept': 'application/json' })
        .send({
          "firstName": "ManPost",
          "phoneNum": "0700000000",
          "email": "postman@email.com",
          "password": "pass",
          "address": "address",
          "city": "citi",
          "postalCode": "00000"
        });

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putUser));
    });
  });

  describe(`DELETE users/user/:id =>`, () => {
    test(`Delete a user with id`, async () => {
      const res = await request(baseUrl)
        .delete(`/users/user/${getUser[0].id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putUser));
    });
  });
});
